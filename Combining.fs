﻿module Combining

open System


let comp1 : Async<int> = 
    async { return 1 }

let comp2 (a : int) : Async<int> = 
    async { return a + 1 }

let comp3 a b : Async<int> =
    async {
        try
            failwith "some exception" 
            return a + b
        with
            | x-> printfn "Exception: %s" x.Message
                  return 0
    }


let combiningSequentially() =
    async {
        let! x = comp1
        let! y = comp2 x
        return x + y
    }
    |> Async.RunSynchronously
    |> printfn "combiningSequencially comp1 & comp2:%A"

let combiningParallel() =
    let results = 
        [comp1; comp2 10]
        |> Async.Parallel
        |> Async.RunSynchronously

    results
    |> Seq.iter (printfn  "combining parallel: %A")

let exceptionHandling() =
    let failingComputation : Async<unit> =
        async { failwith "some exception" }
    
    async {
        try
            do! failingComputation
            return 10
        with | ex -> return 0
    }
    |> Async.RunSynchronously
    |> printfn "exception handling :%d"

let main()=
    combiningSequentially()
    combiningParallel()
    exceptionHandling()