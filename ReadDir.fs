﻿module ReadDir

open System
open System.IO

let private readFileAsync (file:string) : Async<byte[]>=
  async {
    // Open stream
    use stream = File.OpenRead file

    // Async read, so we don't block on the thread pool
    let! content = stream.AsyncRead(int stream.Length)

    // Return data
    return content
  }

let rec private listDir (dir:string) : string seq=
    //list files in current dir
    let files = 
        Directory.GetFiles dir 
        |> Seq.ofArray

    // Collect (i.e. SelectMany/flatMap) all the files in subdirectories
    let subDirs = Directory.GetDirectories dir
                    |> Seq.collect listDir 

    Seq.concat [files; subDirs]

let processFileAsync (f:byte[] -> Async<'a>) (path:string) : Async<string * 'a>=
    async {
        let! fileContent = readFileAsync path
        let! result = f fileContent
        return (path, result)
    }

let countBytes (bs : byte[]) = async { return bs.Length }

let main() =
    listDir "."
    |> Seq.map (processFileAsync countBytes)
    |> Array.ofSeq
    |> Async.Parallel
    |> Async.RunSynchronously
    |> printfn "ReadDir results: %A"