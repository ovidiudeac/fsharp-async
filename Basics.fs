﻿module Basics

open System


let comp1 : Async<int> = 
    async { return 1 }

let comp2 (a : int) : Async<int> = 
    async { return a + 1 }

let comp3 a b : Async<int> =
    async {
        try
            failwith "some exception" 
            return a + b
        with
            | x-> printfn "Exception: %s" x.Message
                  return 0
    }

let simple() : unit =
    let a = comp1
    printfn "a=%A" a

    let b = comp2 10
    printfn "b=%A" b

    let c = comp3 1 2
    printfn "c=%A" c


let executing() =
    comp1
    |> Async.RunSynchronously
    |> printfn "comp1=%A"
    
    comp2 10
    |> Async.RunSynchronously
    |> printfn "comp2 10=%A"
    
    comp3 1 2
    |> Async.RunSynchronously
    |> printfn "comp3 1 2 =%A"

let main()=
    simple()
    executing()