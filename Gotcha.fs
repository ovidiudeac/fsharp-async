﻿module Gotchas

open Common

module v1 =
    let workThenWait() = 
        heavyWork()
        printfn "work done"
        async { do! Async.Sleep(1000) }

    let demo() = 
        let work = 
            workThenWait()
            |> Async.StartAsTask

        printfn "started"
        work.Wait()
        printfn "completed"


module v2 =
    let workThenWait() = 
        async { 
            heavyWork()
            printfn "work done"
            do! Async.Sleep(1000) 
        }